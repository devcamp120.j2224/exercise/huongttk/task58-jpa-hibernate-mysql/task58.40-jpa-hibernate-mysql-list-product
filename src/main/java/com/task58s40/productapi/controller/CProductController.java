package com.task58s40.productapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task58s40.productapi.model.CProduct;
import com.task58s40.productapi.repository.CProductRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductController {
    @Autowired
    CProductRepository productRepository;
    @GetMapping("products")
    public ResponseEntity <List<CProduct>> getAllProduct (){
        try {
            List<CProduct> listProduct = new ArrayList<CProduct>();
            productRepository.findAll().forEach(listProduct :: add);
            return new ResponseEntity<>(listProduct, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
